/** @type {import('tailwindcss').Config} */
export default {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {
      opacity: {
        15: ".15",
      },
      lineClamp: {
        7: "7",
        8: "8",
        9: "9",
        10: "10",
      },
    },
    boxShadow: {
      'loan-form': '0px 0px 30px rgba(0, 0, 0, 0.2)',
    },
    backgroundImage: {
      'vector-banner': "url('/imgs/downloads/vector.png')",
    },
    backgroundPosition: {
      'bottom-4': 'center bottom 1rem',
    },
    colors: {
      "white": "#fff",
      "primary-50": "#FFE0C0",
      "primary-100": "#FFD5AB",
      "primary-200": "#FFC78F",
      "primary-300": "#FFB46A",
      "primary-400": "#FF9C38",
      "primary-500": "#F57C00",
      "primary-600": "#D06800",
      "primary-700": "#B15800",
      "primary-800": "#964B00",
      "primary-900": "#804000",
      "secondary-50": "#C1C1EC",
      "secondary-100": "#ADACE6",
      "secondary-200": "#9190DD",
      "secondary-300": "#6D6BD2",
      "secondary-400": "#3D3BC2",
      "secondary-500": "#282780",
      "secondary-600": "#22216D",
      "secondary-700": "#1D1C5C",
      "secondary-800": "#19184F",
      "secondary-900": "#151443",
      "error-50": "#F5C4C2",
      "error-100": "#F1B1AD",
      "error-200": "#ED9792",
      "error-300": "#E7746E",
      "error-400": "#DF463E",
      "error-500": "#B3261E",
      "error-600": "#98201A",
      "error-700": "#811B16",
      "error-800": "#6E1712",
      "error-900": "#5D1410",
      "success-50": "#A6FFD3",
      "success-100": "#88FFC4",
      "success-200": "#61FFB0",
      "success-300": "#2CFF96",
      "success-400": "#00E572",
      "success-500": "#008744",
      "success-600": "#007339",
      "success-700": "#006231",
      "success-800": "#005329",
      "success-900": "#004723",
      "grey-bg": "#F5F5F5",
      "disable-bg": "#E0E0E0",
      "900-ti": "#212529",
      "800-ti": "#333333",
      "700-ti": "#666666",
      "disable-ti": "#B5B5B5",
      "grey-bor": "#E1E1E1"
    },
    screens: {
      xsm: "400px",
      // => @media (min-width: 400px) { ... }

      sm: "640px",
      // => @media (min-width: 640px) { ... }

      md: "768px",
      // => @media (min-width: 768px) { ... }

      lg: "1024px",
      // => @media (min-width: 1024px) { ... }

      xl: "1280px",
      // => @media (min-width: 1280px) { ... }

      "2xl": "1536px",
      // => @media (min-width: 1536px) { ... }
    },
  },
  plugins: [],
};
