import { UtilsConst } from "@/lib/definitions/UtilsConst";

function IconFacebook(props) {
    const {
        width = UtilsConst.variableCSS.sizeIconFooter,
        height = UtilsConst.variableCSS.sizeIconFooter,
        color = "#666666",
    } = props;
    return (
        <svg width={width} height={height} viewBox="0 0 50 50" fill="none" xmlns="http://www.w3.org/2000/svg">
            <circle cx="25" cy="25" r="24.5" stroke={color} />
            <path d="M27.5 26.875H30.625L31.875 21.875H27.5V19.375C27.5 18.0875 27.5 16.875 30 16.875H31.875V12.675C31.4675 12.6213 29.9287 12.5 28.3037 12.5C24.91 12.5 22.5 14.5712 22.5 18.375V21.875H18.75V26.875H22.5V37.5H27.5V26.875Z" fill={color} />
        </svg>

    );
}
export default IconFacebook;
