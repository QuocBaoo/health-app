import { UtilsConst } from "@/lib/definitions/UtilsConst";

function IconMail(props) {
    const {
        width = UtilsConst.variableCSS.sizeIconFooter,
        height = UtilsConst.variableCSS.sizeIconFooter,
        color = "#666666",
    } = props;
    return (
        <svg width={width} height={height} viewBox="0 0 50 50" fill="none" xmlns="http://www.w3.org/2000/svg">
            <circle cx="25" cy="25" r="24.5" stroke={color} />
            <path d="M35 32.5H32.5V21.5625L25 26.25L17.5 21.5625V32.5H15V17.5H16.5L25 22.8125L33.5 17.5H35M35 15H15C13.6125 15 12.5 16.1125 12.5 17.5V32.5C12.5 33.163 12.7634 33.7989 13.2322 34.2678C13.7011 34.7366 14.337 35 15 35H35C35.663 35 36.2989 34.7366 36.7678 34.2678C37.2366 33.7989 37.5 33.163 37.5 32.5V17.5C37.5 16.837 37.2366 16.2011 36.7678 15.7322C36.2989 15.2634 35.663 15 35 15Z" fill={color} />
        </svg>
    );
}
export default IconMail;
