import IconSearch from "@/icons/iconSearch";

function InputSearch() {
  return (
    <div className="w-[343px] bg-grey-bg h-[30px] flex items-center justify-center pl-4 rounded-3xl pr-2">
      <span className="pl-2 pr-4">
        <IconSearch />
      </span>
      <input
        type="text"
        name="q"
        className="w-full bg-grey-bg focus:outline-none"
        placeholder="Tìm kiếm cơ hội việc làm"
      />
    </div>
  );
}

export default InputSearch;
