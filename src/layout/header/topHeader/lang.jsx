function Lang() {
  return (
    <div className="flex items-center ml-3.5 bg-grey-bg pr-2 rounded-3xl">
      <img src="/imgs/lang/vietnames.png" alt="Đa ngôn ngữ" />
      <span className="ml-2">VN</span>
    </div>
  );
}

export default Lang;
