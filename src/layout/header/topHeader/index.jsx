import InputSearch from "./inputSearch";
import Lang from "./lang";

function TopHeader() {
  return (
    <div className="min-h-[50px] mx-auto flex flex-row-reverse items-center border-b border-grey-bor">
      <div className="min-w-[1232px] mx-auto flex flex-row-reverse">
        <Lang />
        <InputSearch />
      </div>
    </div>
  );
}

export default TopHeader;
