import { useLocation } from "react-router-dom";
import clsx from "clsx";

function NavItemDef(props) {
  const { itemNav } = props;
  const location = useLocation();
  const { pathname } = location;

  return (
    <div
      className={clsx(
        "inline-flex items-center gap-3 px-4 py-1 rounded hover:bg-primary-500 hover:bg-opacity-10",
        pathname === itemNav?.link ? "text-900-ti" : "text-700-ti"
      )}
    >
      <span className="text-base font-semibold hidden sm:block uppercase">
        {itemNav?.title}
      </span>
    </div>
  );
}

export default NavItemDef;
