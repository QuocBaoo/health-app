import { Link } from "react-router-dom";
import { LazyLoadImage } from "react-lazy-load-image-component";
import { UtilsConst } from "@/lib/definitions/UtilsConst";
import NavDef from "./nav/navDef";
import ButtonLogin from "../../components/common/ButtonLogin";
import TopHeader from "./topHeader";

function HeaderDefault() {
  return (
    <header className="bg-white">
      <TopHeader />
      <div className="max-w-[1232px] min-height-header mx-auto flex items-center">
        <div className="ml-4">
          <Link to={UtilsConst?.page.homePage.url}>
            <LazyLoadImage
              className="w-[85px] h-[43px] cursor-pointer sm:w-[200px] select-none"
              src="/imgs/logo.svg"
              alt="Logo SHB Finance Header"
            />
          </Link>
        </div>
        <div className="flex items-center ml-auto gap-2 sm:gap-14">
          <NavDef />
          <ButtonLogin />
        </div>
      </div>
    </header>
  );
}

export default HeaderDefault;
