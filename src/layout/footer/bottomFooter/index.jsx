import LogoFooter from "./logoFooter";
import ContentFooter from "./contentFooter";
import DownloadApp from "./downloadApp";

function BottomFooter() {
    return (
        <div className="flex items-center justify-between py-[55px] px-16">
            <LogoFooter />
            <ContentFooter />
            <DownloadApp />
        </div>
    );
}

export default BottomFooter;
