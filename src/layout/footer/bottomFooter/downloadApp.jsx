import ListLogoDown from "@/components/common/ListLogoDown";

function DownloadApp() {
  return (
    <div className="flex flex-col text-700-ti ml-[30px]">
      <div className="text-3xl font-semibold mb-[44px]">Tải ứng dụng</div>
      <ListLogoDown />
    </div>
  );
}

export default DownloadApp;
