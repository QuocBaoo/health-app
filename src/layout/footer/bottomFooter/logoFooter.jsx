import { Link } from "react-router-dom";
import { LazyLoadImage } from "react-lazy-load-image-component";
import { UtilsConst } from "@/lib/definitions/UtilsConst";
import IconMail from "@/icons/iconMail";
import IconFacebook from "@/icons/iconFacebook";
import IconYoutube from "@/icons/iconYoutube";
import IconZalo from "@/icons/iconZalo";

function LogoFooter() {
  return (
    <div className="flex flex-col mr-[50px] min-w-[275px]">
      <Link to={UtilsConst?.page.homePage.url}>
        <LazyLoadImage
          className="w-[85px] h-[52px] cursor-pointer sm:w-[232px] select-none"
          src="/imgs/logo.svg"
          alt="Logo SHB Finance Footer"
        />
      </Link>
      <div className="text-700-ti text-3xl font-semibold mt-[46px]">
        Hotline: 1900 2198
      </div>
      <div className="flex justify-between mt-[15px]">
        <span className="cursor-pointer">
          <IconFacebook />
        </span>
        <span className="cursor-pointer">
          <IconYoutube />
        </span>
        <span className="cursor-pointer">
          <IconMail />
        </span>
        <span className="cursor-pointer">
          <IconZalo />
        </span>
      </div>
    </div>
  );
}

export default LogoFooter;
