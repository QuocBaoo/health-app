
function ContentFooter() {
    return (
        <div className="flex flex-col max-w-[540px] text-black">
            <div className="text-3xl font-semibold">Công ty Tài chính TNHH MTV Ngân hàng TMCP Sài Gòn – Hà Nội</div>
            <div className="text-lg mt-[18px]">Địa chỉ: Tầng 6, Gelex Tower, 52 Lê Đại Hành, phường Lê Đại Hành, Quận Hai Bà Trưng, Hà Nội.</div>
            <div className="mt-[32px] text-lg">
                <span>Điện thoại: 8769798644322390</span>
                <br />
                <span>Fax: 87554439080-0</span>
                <br />
                <span>Email: cskh@shbfinance.com.vn</span>
            </div>
        </div>
    );
}

export default ContentFooter;
