import { DataFooter } from "@/lib/data/dataFooter";
import { Link } from "react-router-dom";
import BottomFooter from "./bottomFooter";

function FooterDef() {
  const dataFooter = DataFooter.listFooter;
  return (
    <footer className="bg-grey-bg">
      <div className="max-w-[1440px] mx-auto min-height-footer text-800-ti text-xl flex items-center justify-center font-bold solid border-b border-disable-ti">
        {dataFooter?.map((itemFooter, index) => (
          <Link to={itemFooter?.link} key={index}>
            <span className="hover:text-primary-500 mx-4">
              {itemFooter?.title}
            </span>
          </Link>
        ))}
      </div>
      <BottomFooter />
    </footer>
  );
}

export default FooterDef;
