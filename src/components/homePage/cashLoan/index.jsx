import ButtonApplyLoan from "@/components/common/ButtonApplyLoan";

function LoanProcess() {
  return (
    <div className="my-16 bg-[#FDF6EA] p-[40px]">
      <div className="uppercase text-[#262626] text-4xl font-semibold text-center">
        Vay tiền mặt
      </div>
      <div className="flex justify-center items-center gap-x-6 sm:gap-x-8 md:gap-x-14 lg:gap-x-[84px] my-6">
        <div className="flex flex-col justify-end items-center h-[320px] w-[242px]">
          <img src="/imgs/cashLoan/img1.png" alt="Duyệt vay đến 70 triệu" />
          <div className="font-medium	text-center text-lg">
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry.
          </div>
        </div>
        <div className="flex flex-col justify-end items-center h-[320px] w-[242px]">
          <img src="/imgs/cashLoan/img1.png" alt="Duyệt vay đến 70 triệu" />
          <div className="font-medium	text-center text-lg">
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry.
          </div>
        </div>
        <div className="flex flex-col justify-end items-center h-[320px] w-[242px]">
          <img src="/imgs/cashLoan/img1.png" alt="Duyệt vay đến 70 triệu" />
          <div className="font-medium	text-center text-lg">
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry.
          </div>
        </div>
      </div>
      <div className="text-center">
        <ButtonApplyLoan />
      </div>
    </div>
  );
}

export default LoanProcess;
