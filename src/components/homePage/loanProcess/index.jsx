function CashLoan() {
    return (
        <div className="my-16">
            <div className="uppercase text-[#262626] text-4xl font-semibold">Vay tiền mặt</div>
            <div className="flex justify-center items-center gap-x-6 sm:gap-x-8 md:gap-x-14 lg:gap-x-[84px] my-6">
                <div className="flex flex-col justify-end items-center h-[320px]">
                    <img src="/imgs/cashLoan/img1.png" alt="Duyệt vay đến 70 triệu" />
                    <div className="font-bold text-center text-[30px]">
                        <span className="text-black">Duyệt vay đến</span>
                        <br />
                        <span className="text-[#F38000]">70 triệu</span>
                    </div>
                </div>
                <div className="flex flex-col justify-end items-center h-[320px]">
                    <div>
                        <img src="/imgs/cashLoan/img2.png" alt="Giải ngân nhanh chóng" />
                    </div>
                    <div className="font-bold text-center text-[30px]">
                        <span className="text-black">Giải ngân</span>
                        <br />
                        <span className="text-[#F38000]">nhanh chóng</span>
                    </div>
                </div>
                <div className="flex flex-col justify-end items-center h-[320px]">
                    <img src="/imgs/cashLoan/img3.png" alt="Thủ tục đơn giản, minh bạch" />
                    <div className="font-bold text-center text-[30px]">
                        <span className="text-black">Thủ tục</span>
                        <span className="text-[#F38000] ml-2">đơn giản,</span>
                        <br />
                        <span className="text-[#F38000]">minh bạch</span>
                    </div>
                </div>
                <div className="flex flex-col justify-end items-center h-[320px]">
                    <img src="/imgs/cashLoan/img4.png" alt="Thủ tục đơn giản, minh bạch" />
                    <div className="font-bold text-center text-[30px]">
                        <span className="text-black">Thủ tục</span>
                        <span className="text-[#F38000] ml-2">đơn giản,</span>
                        <br />
                        <span className="text-[#F38000]">minh bạch</span>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default CashLoan;