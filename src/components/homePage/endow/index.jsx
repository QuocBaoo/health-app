function Endow() {
  return (
    <div className="my-16">
      <div className="uppercase text-[#262626] text-4xl font-semibold">
        ƯU ĐÃI DÀNH CHO BẠN
      </div>
      <div className="flex justify-center items-start gap-x-6 sm:gap-x-8 md:gap-x-14 lg:gap-x-[25px] my-16">
        <div className="flex flex-col justify-start items-center w-[370px]">
          <img className="h-[234px]" src="/imgs/endow/img1.png" alt="Ưu đãi dành cho bạn" />
          <div className="text-center pt-[14px] px-16">
            <div className="text-black text-[14px] mt-3 text-left">
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard
            </div>
          </div>
        </div>
        <div className="flex flex-col justify-start items-center w-[370px]">
          <img className="h-[234px]" src="/imgs/endow/img2.png" alt="Ưu đãi dành cho bạn" />
          <div className="text-center pt-[14px] px-16">
            <div className="text-black text-[14px] mt-3 text-left">
            Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
            </div>
          </div>
        </div>
        <div className="flex flex-col justify-start items-center w-[370px]">
          <img className="h-[234px]" src="/imgs/endow/img1.png" alt="Ưu đãi dành cho bạn" />
          <div className="text-center pt-[14px] px-16">
            <div className="text-black text-[14px] mt-3 text-left">
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Endow;
