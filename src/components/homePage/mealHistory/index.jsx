import { useState } from "react";
import HistoryCom from "./history";
import MenuHistoryCom from "./menuHistory";
import CashLoan from "../cashLoan";
import LoanForm from "../loanForm";
import BannerDownload from "../bannerDownload";
import Endow from "../endow";
import LoanProcess from "../cashLoan";

function MealHistory() {
  const [type, setType] = useState(null);
  return (
    <div className="max-w-[1285px] m-auto">
      <CashLoan />
      <LoanForm />
      <BannerDownload />
      <Endow />
      <LoanProcess />
      {/* <MenuHistoryCom setType={setType} />
      <HistoryCom type={type} /> */}
    </div>
  );
}

export default MealHistory;
