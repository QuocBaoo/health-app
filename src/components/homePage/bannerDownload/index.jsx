import ListLogoDown from "@/components/common/ListLogoDown";

function BannerDownload() {
  return (
    <div className="my-16 bg-[#DADADA33] bg-vector-banner bg-no-repeat	bg-bottom-4 rounded-[20px] w-[1290px] h-[480px] flex justify-start items-center  gap-x-6 sm:gap-x-8 md:gap-x-14 lg:gap-x-[84px] ">
      <div className="w-1/2">
        <img
          className="mx-auto h-[300px] w-[533px]"
          src="/imgs/downloads/banner.png"
          alt="banner download"
        />
      </div>
      <div className="w-1/2 max-w-[359px] flex flex-col justify-center items-start h-[320px] text-800-ti">
        <div className="font-bold text-4xl">
          Tải App chính chủ không lo mất của
        </div>
        <div className="font-medium mt-[70px] mb-[32px]">Tải ứng dụng miễn phí tại</div>
        <ListLogoDown cls="justify-between" />
      </div>
    </div>
  );
}

export default BannerDownload;
