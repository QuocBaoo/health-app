function LoanForm() {
  return (
    <div className="my-16">
      <div className="uppercase text-[#262626] text-4xl font-semibold">
        Hình thức vay
      </div>
      <div className="flex justify-center items-center gap-x-6 sm:gap-x-8 md:gap-x-14 lg:gap-x-[25px] my-16">
        <div className="flex flex-col justify-start items-center h-[573px] w-[368px] shadow-loan-form">
          <img src="/imgs/loanForm/img1.png" alt="Hình thức vay" />
          <div className="text-left p-5">
            <span className="text-black font-bold text-[20px]">
              Vay theo bảng lương
            </span>
            <div className="text-[#979797] mt-3">
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard
            </div>
          </div>
          <div className="border-t border-grey-bor h-[70px] w-full px-6 flex items-center">
            <button className="border border-solid rounded-xl border-primary-500 px-4 py-2">
              <span className="text-primary-500 font-bold text-sm">
                Do something
              </span>
            </button>
          </div>
        </div>
        <div className="flex flex-col justify-start items-center h-[573px] w-[368px] shadow-loan-form">
          <img src="/imgs/loanForm/img1.png" alt="Hình thức vay" />
          <div className="text-left p-5">
            <span className="text-black font-bold text-[20px]">
              Vay theo bảng lương
            </span>
            <div className="text-[#979797] mt-3">
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard
            </div>
          </div>
          <div className="border-t border-grey-bor h-[70px] w-full px-6 flex items-center">
            <button className="border border-solid rounded-xl border-primary-500 px-4 py-2">
              <span className="text-primary-500 font-bold text-sm">
                Do something
              </span>
            </button>
          </div>
        </div>
        <div className="flex flex-col justify-start items-center h-[573px] w-[368px] shadow-loan-form">
          <img src="/imgs/loanForm/img1.png" alt="Hình thức vay" />
          <div className="text-left p-5">
            <span className="text-black font-bold text-[20px]">
              Vay theo bảng lương
            </span>
            <div className="text-[#979797] mt-3">
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard
            </div>
          </div>
          <div className="border-t border-grey-bor h-[70px] w-full px-6 flex items-center">
            <button className="border border-solid rounded-xl border-primary-500 px-4 py-2">
              <span className="text-primary-500 font-bold text-sm">
                Do something
              </span>
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default LoanForm;
