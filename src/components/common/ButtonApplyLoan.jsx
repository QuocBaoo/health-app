function ButtonApplyLoan() {
  return (
    <button className="border border-solid rounded-xl border-primary-500 bg-[#F38000] px-5 py-3 w-[230px]">
      <span className="font-bold text-white text-base">Đăng ký vay</span>
    </button>
  );
}

export default ButtonApplyLoan;
