function ButtonLogin() {
  return (
    <button className="border border-solid rounded-xl border-primary-500 px-4 py-2">
      <span className="text-primary-500 font-bold text-sm">Đăng nhập</span>
    </button>
  );
}

export default ButtonLogin;
