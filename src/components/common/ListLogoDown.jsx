import clsx from "clsx";

function ListLogoDown(props) {
  const { cls = "justify-evenly" } = props;
  return (
    <div className="flex justify-center">
      <div className={clsx("flex flex-col items-center mr-[17px]", cls)}>
        <div className="cursor-pointer">
          <img src="/imgs/downloads/appStore.svg" alt="appStore download" />
        </div>
        <div className="cursor-pointer">
          <img src="/imgs/downloads/googlePlay.svg" alt="googlePlay download" />
        </div>
      </div>
      <img src="/imgs/downloads/qr-mcredit.svg" alt="qr-mcredit download" />
    </div>
  );
}

export default ListLogoDown;
