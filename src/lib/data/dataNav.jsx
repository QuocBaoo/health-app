import { UtilsConst } from "../definitions/UtilsConst";

export const DataNav = {
  listNav: [
    {
      title: "Khuyến mãi",
      link: UtilsConst?.page.recordPage.url,
    },
    {
      title: "Sản phẩm",
      link: "#",
    },
    {
      title: "Đầu tư",
      link: "#",
    },
    {
      title: "Ứng dụng SHB Finance",
      link: "#",
    },
    {
      title: "Tra cứu thông tin",
      link: "#",
    },
  ],
};
