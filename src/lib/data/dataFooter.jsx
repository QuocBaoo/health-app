import { UtilsConst } from "../definitions/UtilsConst";

export const DataFooter = {
  listFooter: [
    {
      title: "Câu hỏi thường gặp",
      link: UtilsConst?.page.homePage.url,
    },
    {
      title: "Điều khoản sử dụng",
      link: UtilsConst?.page.homePage.url,
    },
    {
      title: "Về SHB Finance",
      link: UtilsConst?.page.homePage.url,
    },
    {
      title: "Tin tức chung",
      link: UtilsConst?.page.homePage.url,
    },
    {
      title: "Cơ hội nghề nghiệp",
      link: UtilsConst?.page.homePage.url,
    },
  ],
};
